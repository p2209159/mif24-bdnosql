// Codage d'un interpréteur JSONPath en Javascript
// Structure de données, cf TD

/**
 * Renvoie le tableau contenant le noeud de départ, ainsi que tout ses descendants
 * @param {*} j le noeud json de départ
 */
function sousDocs(j) {
  if (typeof j === "object") {
    let desc = Object.values(j).flatMap((j2) => sousDocs(j2));
    desc.push(j);
    return desc;
  } else {
    return j !== undefined ? [j] : [];
  }
}

/**
 * Évalue l'expression JSON et renvoie la liste des noeud de l'arbre sélectionné par l'expression.
 * @param {Object} r la racine du document JSON interrogé
 * @param {Object} c un document JSON de contexte (pour @)
 * @param {Object} exprC un objet représentant l'expression à évaluer
 * @throws Expression mal définie
 * @returns un tableau de valeurs (objets JSON et/ou valeurs atomiques)
 */
function evalC(r, c, exprC) {
  // Par cas sur l'opérateur JSON Path
  switch (exprC.op) {
    case "$":
      return [r];

    case "@":
      return [c];

    case ".": {
      if (
        typeof exprC.expr !== "object" ||
        !(typeof exprC.field === "string" || typeof exprC.field === "number")
      ) {
        throw new Error(`Expression mal définie`);
      }
      let prevStep = evalC(r, c, exprC.expr);
      return prevStep
        .map((j) => (typeof j === "object" ? j[exprC.field] : undefined))
        .filter((j2) => j2 !== undefined);
    }

    case "*": {
      if (typeof exprC.expr !== "object") {
        throw new Error(`Expression mal définie`);
      }
      let prevStep = evalC(r, c, exprC.expr);
      return prevStep
        .flatMap((j) => (typeof j === "object" ? Object.values(j) : []))
        .filter((j2) => j2 !== undefined);
    }

    case "..": {
      if (typeof exprC.expr !== "object") {
        throw new Error(`Expression mal définie`);
      }
      let prevStep = evalC(r, c, exprC.expr);
      return prevStep.flatMap(sousDocs);
    }

    case "?": {
      if (typeof exprC.expr !== "object" || typeof exprC.cond !== "object") {
        throw new Error(`Expression mal définie`);
      }
      let prevStep = evalC(r, c, exprC.expr);
      return prevStep.filter((j) => evalCond(r, j, exprC.cond));
    }

    default:
      throw new Error(`Opérateur JSON Path non géré: ${exprC.op}`);
  }
}

exports.evalC = evalC;

/**
 * Compare deux valeurs selon l'opérateur choisi
 * @param {string} op l'opérateur de comparaison
 * @param {*} v1 la première valeur
 * @param {*} v2 la deuxième valeur
 * @returns le résultat de la comparaison des deux valeurs
 */
function cmp(op, v1, v2) {
  switch (op) {
    case "==":
      return v1 == v2;
    case "<":
      return v1 < v2;
    case ">":
      return v1 > v2;
    case "<=":
      return v1 <= v2;
    case ">=":
      return v1 >= v2;
    case "!=":
      return v1 != v2;
    case "<>":
      return v1 != v2;
    default:
      throw new Error("Comparateur inconnu: " + op);
  }
}

/**
 * Évalue une condition JSON Path
 * @param {*} r le document requêté
 * @param {*} c le noeud de contexte
 * @param {Object} exprB la condition à évaluer
 * @returns true si la condition s'évalue à vrai dans le contexte fourni
 */
function evalCond(r, c, exprB) {
  switch (exprB.op) {
    case "true":
      return true;

    case "false":
      return false;

    case "!":
      if (typeof exprB.expr !== "object") {
        throw new Error(`Condition mal définie`);
      }
      return !evalCond(r, c, exprB.expr);

    case "&&":
      if (typeof exprB.expr1 !== "object" || typeof exprB.expr2 !== "object") {
        throw new Error(`Condition mal définie`);
      }
      return evalCond(r, c, exprB.expr1) && evalCond(r, c, exprB.expr2);

    case "||":
      if (typeof exprB.expr1 !== "object" || typeof exprB.expr2 !== "object") {
        throw new Error(`Condition mal définie`);
      }
      return evalCond(r, c, exprB.expr1) || evalCond(r, c, exprB.expr2);

    case "cmp_val": {
      if (typeof exprB.expr !== "object") {
        throw new Error(`Condition mal définie`);
      }
      let candidates = evalC(r, c, exprB.expr);
      return candidates.some((j) => cmp(exprB.cmp, j, exprB.val));
    }

    case "cmp_expr": {
      if (typeof exprB.expr1 !== "object" || typeof exprB.expr2 !== "object") {
        throw new Error(`Condition mal définie`);
      }
      let candidates1 = evalC(r, c, exprB.expr1);
      let candidates2 = evalC(r, c, exprB.expr2);
      return candidates1.some((j1) =>
        candidates2.some((j2) => cmp(exprB.cmp, j1, j2))
      );
    }

    case "exists":
      if (typeof exprB.expr !== "object") {
        throw new Error(`Condition mal définie`);
      }
      return evalC(r, c, exprB.expr).length > 0;

    default:
      throw new Error(`Opérateur de condition JSON Path non géré: ${exprB.op}`);
  }
}

exports.evalCond = evalCond;

// Builder d'expressions
let cheminPrototype = {};

cheminPrototype.at = function (champ) {
  let res = Object.create(cheminPrototype);
  res.field = champ;
  res.op = ".";
  res.expr = this;
  return res;
};

cheminPrototype.children = function () {
  let res = Object.create(cheminPrototype);
  res.op = "*";
  res.expr = this;
  return res;
};

cheminPrototype.desc = function () {
  let res = Object.create(cheminPrototype);
  res.op = "..";
  res.expr = this;
  return res;
};

cheminPrototype.cond = function (cond) {
  let res = Object.create(cheminPrototype);
  res.op = "?";
  res.expr = this;
  res.cond = cond;
  return res;
};

let root = Object.create(cheminPrototype);
root.op = "$";
exports.root = root;

let ctx = Object.create(cheminPrototype);
ctx.op = "@";
exports.ctx = ctx;

exports.cTrue = { op: "true" };
exports.cFalse = { op: "false" };
exports.and = function (e1, e2) {
  return { op: "&&", expr1: e1, expr2: e2 };
};
exports.or = function (e1, e2) {
  return { op: "||", expr1: e1, expr2: e2 };
};
exports.not = function (e) {
  return { op: "!", expr: e };
};
exports.cmpV = function (op, e, v) {
  return { op: "cmp_val", expr: e, val: v, cmp: op };
};
exports.cmpC = function (op, e1, e2) {
  return { op: "cmp_expr", expr1: e1, expr2: e2, cmp: op };
};
exports.exists = function (e) {
  return { op: "exists", expr: e };
};
